import java.util.Collection;
import java.util.stream.IntStream;

public class ArrayManipution {
    static int length;
    static int[] numbers = {5,8,3,2,7};
    public static void sortArray(int[] array)
    {
        int temporary = 0;
        for (int i = 0; i < length; i++) {
            for (int j = i + 1; j < length; j++) {
                if (numbers[i] > numbers[j]) {
                    temporary = numbers[i];
                    numbers[i] = numbers[j];
                    numbers[j] = temporary;
                }
            }
        }
    } 
    public static void main(String[] args) {
        int[] numbers = {5,8,3,2,7};
        String[] names = {"Alice", "Bob", "Charlie", "David"};
        double[] values = new double[4];

        System.out.println("Elements of the numbers array: ");
        for(int i = 0; i < numbers.length; i++){
            System.out.println(numbers[i]);
        }

        System.out.println("Elements of the names array: ");
        for(int i = 0; i < names.length; i++){
            System.out.println(names[i]);
        }

        values[0] = 1.1;
        values[1] = 2.2;
        values[2] = 3.3;
        values[3] = 4.4;

        int sum = IntStream.of(numbers).sum();
        System.out.println("The sum is " + sum);

        int max = numbers[0];
        for (int i = 1; i < numbers.length; i++) {
            if (numbers[i] > max) {
                max = numbers[i]; 
            }   
        }
        System.out.println("Maximum value: " + max);

        String[] reversedNames = names;
        for(int i=reversedNames.length-1;i>=0;i--)
        System.out.print(reversedNames[i] + "  ");

        sortArray(numbers);


    }
}